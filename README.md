# Frontend Excercise

This project demonstrates a simple show case of item filtering,
styling and responsiveness with JavaScript.

## Quick start

This repository provides a distribution folder (`dist/`) with a pre-compiled JavaScript bundle.
Just open [./dist/index.html](./dist/index.html) to see the show case.

## Used technologies

- [React](https://reactjs.org) as core technology
- [JSS](https://cssinjs.org/) for component based styling, also known as CSS-in-JS
- [ESLint](https://eslint.org) as code style linter / assurance
- [Babel](https://babeljs.io) as ES2018 to ES5 transpiler
- [webpack](https://webpack.js.org) for bundling
- webpack-dev-server for local development with hot module support

## Development mode

After a fresh git clone or after you've extracted this folder from an
archive, please run

    yarn install

Start up the **development** server using

    yarn start-dev
    
After a few seconds you should be able to visit the app at http://localhost:8080

The app features hot module reload, so it will automatically reload the page after a detected file change.

## Re-Build

Run `yarn build` to build the project. The build artifacts will be
stored in the `dist/` directory.

## Known Issues

- You will still find a `main.css` file. This is only for demonstration purposes and can be removed
 in a refactoring step.
- You will find two JSON files with some data, that should be loaded via AJAX in production. This current
 solution was chosen to keep the project simple. 

## Unit tests

Unfortunately, this project has no unit tests so far. I've planned unit tests 
via [Karma](https://karma-runner.github.io) and end-to-end tests
via [Protractor](http://www.protractortest.org/).

But then I've ran out of time. :'-(

---
created in February 2019 by Michael Rotmanov <michael@rotmanov.de>

