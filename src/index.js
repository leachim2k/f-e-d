import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './main.css';

const mountPointId = 'app';

// root holds our app's root DOM Element:
let root;

function init(mountPoint) {
    root = ReactDOM.render(<App/>, document.getElementById(mountPoint), root);
}

init(mountPointId);

// example: Re-render on Webpack HMR update:
if (module.hot) {
    module.hot.accept('./App', init.bind(null, mountPointId));
}
