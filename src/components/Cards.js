import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import Card from './Card';

const styles = {
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: '-10px'
    },
    cell: {
        flexGrow: 0,
        flexBasis: '25%',
        maxWidth: '25%',
        padding: '10px',
        '@media (max-width: 1200px)': {
            flexBasis: '33%',
            maxWidth: '33%'
        },
        '@media (max-width: 1023px)': {
            flexBasis: '50%',
            maxWidth: '50%'
        },
        '@media (max-width: 425px)': {
            flexBasis: '100%',
            maxWidth: '100%'
        }
    }
};

const Cards = ({ classes, food, availableLabels }) => (
    <div className={classes.root}>
        {food.map(element => (
            <div className={classes.cell} key={element.title}>
                <Card
                    label={availableLabels[element.label] || {}}
                    title={element.title}
                    content={element.content}
                    link={element.link}
                />
            </div>
        ))}
    </div>
);

Cards.propTypes = {
    classes: PropTypes.any,
    availableLabels: PropTypes.object,
    food: PropTypes.array
};

export default injectSheet(styles)(Cards);
