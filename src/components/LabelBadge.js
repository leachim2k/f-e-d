import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import injectSheet from 'react-jss';

const styles = {
    root: {
        backgroundColor: '#777777',
        color: '#ffffff',
        fontSize: '.85em',
        padding: '3px 10px',
        margin: '0 5px',
        borderRadius: '10px',
        whiteSpace: 'nowrap'
    },
    withCheckbox: {
        cursor: 'pointer',
        paddingLeft: '5px'
    }
};

const LabelBadge = ({ classes, className, name, label, color, checked, onChange }) => (
    <label className={classnames(classes.root, className, { [classes.withCheckbox]: !!onChange })}
           style={{ backgroundColor: color }}>
        {onChange && <input type="checkbox" name={name} checked={checked} onChange={onChange}/>}
        {label}
    </label>
);

LabelBadge.propTypes = {
    className: PropTypes.string,
    classes: PropTypes.any,
    name: PropTypes.string,
    label: PropTypes.string.isRequired,
    color: PropTypes.string,
    checked: PropTypes.bool,
    onChange: PropTypes.func
};

export default injectSheet(styles)(LabelBadge);
