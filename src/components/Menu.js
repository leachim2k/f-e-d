import React from 'react';
import classnames from 'classnames';
import injectSheet from 'react-jss';
import PropTypes from 'prop-types';
import items from '../menu.json';

const styles = {
    root: {
        display: 'flex',
        float: 'right',
        '@media (max-width: 768px)': {
            flexDirection: 'column'
        }
    },
    items: {
        padding: 5,
        '& a': {
            textDecoration: 'none'
        }
    },
    activeLink: {
        textDecoration: 'underline'
    }
};

const Menu = ({ classes }) => (
    <ul className={classes.root}>
        {items.map(({ link, label }, index) => (
            <li key={index} className={classnames(classes.items, { [classes.activeLink]: index === 0 })}>
                <a href={link}>{label}</a>
            </li>
        ))}
    </ul>
);

Menu.propTypes = { classes: PropTypes.any };

export default injectSheet(styles)(Menu);
