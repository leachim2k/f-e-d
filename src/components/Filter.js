import React from 'react';
import injectSheet from 'react-jss';
import LabelBadge from './LabelBadge';
import PropTypes from 'prop-types';

const styles = {
    root: {
        margin: '20px 0'
    },
    labelContainer: {
        display: 'inline-flex',
        flexWrap: 'wrap'
    }
};

const Filter = ({ classes, availableLabels, selectedLabels, onChange }) => (
    <div className={classes.root}>
        Filter:
        <div className={classes.labelContainer}>
        {Object.keys(availableLabels).map(labelKey => (
            <LabelBadge key={labelKey} name={labelKey}
                        label={availableLabels[labelKey].name}
                        color={availableLabels[labelKey].color}
                        checked={selectedLabels.has(labelKey)}
                        onChange={onChange}/>
        ))}
        </div>
    </div>
);

Filter.propTypes = {
    classes: PropTypes.any,
    availableLabels: PropTypes.any,
    selectedLabels: PropTypes.objectOf(Set),
    onChange: PropTypes.func
};

export default injectSheet(styles)(Filter);
