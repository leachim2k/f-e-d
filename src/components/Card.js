import React from 'react';
import injectSheet from 'react-jss';

import PropTypes from 'prop-types';
import FilterLabelBadge from './LabelBadge';

const styles = {
    root: {
        background: 'white',
        boxShadow: '0 0 4px 0px rgba(0,0,0,.2)',
    },
    cardMedia: {
        backgroundColor: '#777777',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        height: '140px'
    },
    cardHeader: {
        display: 'flex',
        alignItems: 'center'
    },
    cardTitle: {
        margin: 0,
        padding: 0,
        flex: 1,
        fontWeight: '400',
    },
    cardLabel: {
        float: 'right',
        flex: 0
    },
    cardContent: {
        padding: '20px 20px 30px 20px'
    },
    moreInformationArrow: {
        fontSize: '25px',
        paddingRight: '5px'
    },
    moreInformationLink: {
        textDecoration: 'none',
        '&:hover': {
            textDecoration: 'underline',
        }
    }
};

const Card = ({ classes, label, title, content, link }) => (
    <div className={classes.root}>
        <div className={classes.cardMedia} style={{ backgroundImage: 'url(http://lorempixel.com/400/400/)' }}
             title={'Image for ' + title}/>

        <div className={classes.cardContent}>
            <div className={classes.cardHeader}>
                <h2 className={classes.cardTitle}>
                    {title}
                </h2>
                <FilterLabelBadge className={classes.cardLabel} label={label.name} color={label.color}/>
            </div>

            <p>{content}</p>

            <div>
                <span className={classes.moreInformationArrow} style={{ color: label.color }}>›</span>
                <a href={link} className={classes.moreInformationLink}>More Info</a>
            </div>
        </div>
    </div>
);

Card.propTypes = {
    classes: PropTypes.object,
    label: PropTypes.object,
    title: PropTypes.string,
    content: PropTypes.string,
    link: PropTypes.string,
};

export default injectSheet(styles)(Card);
