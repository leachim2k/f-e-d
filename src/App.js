import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';

// May be fetched dynamically from an URL
import food from './food.json';

import Menu from './components/Menu';
import Filter from './components/Filter';
import Cards from './components/Cards';

const availableLabels = {
    'meat': { name: 'Meat', color: '#E73C01' },
    'vegetable': { name: 'Vegetable', color: '#2B9030' },
    'fruit': { name: 'Fruit', color: '#F39200' },
};

const styles = {
    root: {
        minWidth: '250px',
        maxWidth: '1600px',
        margin: '0 auto'
    },
    header: {
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        '@media (max-width: 768px)': {
            flexDirection: 'column'
        }
    },
    title: {
        flex: 1
    }
};

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedLabels: new Set(Object.keys(availableLabels))
        };
    }

    handleFilterChange = event => {
        const { name, checked } = event.target;
        if (checked) {
            this.state.selectedLabels.add(name);
        } else {
            this.state.selectedLabels.delete(name);
        }
        this.setState({ selectedLabels: this.state.selectedLabels });
    };

    render() {
        return (
            <div className={this.props.classes.root}>
                <header className={this.props.classes.header}>
                    <h1 className={this.props.classes.title}>Brandname</h1>
                    <Menu/>
                </header>

                <Filter availableLabels={availableLabels}
                        selectedLabels={this.state.selectedLabels}
                        onChange={this.handleFilterChange}/>

                <Cards food={food.filter(({ label }) => this.state.selectedLabels.has(label))}
                       availableLabels={availableLabels}/>
            </div>
        );
    }
}

App.propTypes = { classes: PropTypes.any };

export default injectSheet(styles)(App);
